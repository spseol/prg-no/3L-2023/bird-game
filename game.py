from random import randint

import pyglet
from pyglet import gl

from config import START_PLAYER_X, START_PLAYER_Y, GRAVITY, MIN_STONE_Y, MAX_STONE_Y, MAX_BROWN_STONE_TIMER
from objects import Player, Ground, BlueStone, GrayStone


class GameWindow(pyglet.window.Window):
    def __init__(self, *args, **kwargs):
        super(GameWindow, self).__init__(*args, **kwargs)
        self.game_objects = []
        # Load player and ground images
        self.player = Player(img=pyglet.image.load("assets/player.png"), x=START_PLAYER_X, y=START_PLAYER_Y)
        self.ground = Ground(img=pyglet.image.load("assets/ground.png"), x=0, y=0)
        self.score = 0
        self.score_label = pyglet.text.Label(f'Score: {self.score}',
                                             font_name='Arial',
                                             font_size=16,
                                             color=(0, 0, 0, 255),
                                             x=self.width - 150, y=self.height - 30)
        self.life_label = pyglet.text.Label(f'Životy: {self.player.life_counter}',
                                            font_name='Arial',
                                            font_size=16,
                                            color=(0, 0, 0, 255),
                                            x=50, y=self.height - 30)
        self.brown_timer = 0

        # Set up ground properties
        self.ground_y = self.ground.height
        self.is_end = False

    def add_stone(self):
        self.brown_timer += 1
        if self.brown_timer > MAX_BROWN_STONE_TIMER:
            self.game_objects.append(GrayStone(randint(MIN_STONE_Y, MAX_STONE_Y)))
            self.brown_timer = 0
        else:
            self.game_objects.append(BlueStone(randint(MIN_STONE_Y, MAX_STONE_Y)))

    def on_draw(self):
        self.add_stone()
        self.clear()
        self.score_label.draw()
        self.life_label.draw()
        gl.glClearColor(1, 1, 1, 1)
        self.ground.draw()
        self.player.draw()
        for game_object in self.game_objects:
            game_object.draw()

        if self.is_end:
            self.end_label = pyglet.text.Label('Konec hry',
                                               font_name='Arial',
                                               font_size=36,
                                               color=(0, 0, 0, 255),
                                               x=self.width // 2, y=self.height // 2,
                                               anchor_x='center', anchor_y='center')
            self.end_label.draw()

    def update(self, dt):
        if self.is_end:
            return
        self.player.act(self.ground_y, dt)
        for game_object in self.game_objects:
            game_object.act(dt)
            if self.player.collides_with(game_object):
                self.handle_collision(game_object)

    def handle_collision(self, game_object):
        self.game_objects.remove(game_object)
        if isinstance(game_object, GrayStone):
            self.player.life_counter -= 1
            self.life_label.text = f'Životy: {self.player.life_counter}'
            if self.player.life_counter == 0:
                self.is_end = True
        elif isinstance(game_object, BlueStone):
            self.score += 1
            self.score_label.text = f'Score: {self.score}'

    def handle_on_key_press(self, symbol, modifiers):
        if symbol == pyglet.window.key.SPACE:
            self.player.jump()

    def handle_on_key_release(self, symbol, modifiers):
        pass

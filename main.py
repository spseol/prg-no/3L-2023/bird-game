import pyglet
from pyglet.window import key

from config import WINDOW_WIDTH, WINDOW_HEIGHT
from game import GameWindow


if __name__ == "__main__":
    # Set up the game window
    window = GameWindow(width=WINDOW_WIDTH, height=WINDOW_HEIGHT, caption="Bird game")

    # Define key handlers
    @window.event
    def on_key_press(symbol, modifiers):
        window.handle_on_key_press(symbol, modifiers)

    @window.event
    def on_key_release(symbol, modifiers):
        window.handle_on_key_release(symbol, modifiers)

    # Define update function
    pyglet.clock.schedule_interval(window.update, 1/60.0)

    # Run the game
    pyglet.app.run()

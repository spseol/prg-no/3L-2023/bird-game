from random import random

import pyglet

from config import PLAYER_JUMP_SPEED, GRAVITY, WINDOW_WIDTH, WINDOW_HEIGHT, MAX_BLUE_STONE_SPEED, PLAYER_LIFE


class BaseObject(pyglet.sprite.Sprite):
    def __init__(self, *args, **kwargs):
        super(BaseObject, self).__init__(*args, **kwargs)

    def act(self, *args, **kwargs):
        raise NotImplementedError("act method not implemented")

    def collides_with(self, other):
        return (self.x < other.x + other.width and
                self.x + self.width > other.x and
                self.y < other.y + other.height and
                self.y + self.height > other.y)


class Player(BaseObject):
    def __init__(self, *args, **kwargs):
        super(Player, self).__init__(*args, **kwargs)
        self.jump_speed = 400
        self.velocity_y = 0
        self.is_fly = False
        self.scale_x = 0.5
        self.scale_y = 0.5
        self.life_counter = PLAYER_LIFE

    def jump(self):
        self.velocity_y = PLAYER_JUMP_SPEED
        self.is_fly = True

    def act(self, ground_y, dt):
        # Update player position based on velocity
        self.y += self.velocity_y * dt

        # Apply gravity
        self.velocity_y -= GRAVITY * dt

        # Check if the player is on the ground
        if self.y < ground_y:
            self.y = ground_y
            self.velocity_y = 0
            self.is_fly = False


class Ground(BaseObject):
    def __init__(self, *args, **kwargs):
        super(Ground, self).__init__(*args, **kwargs)


class BaseStone(BaseObject):

    def __init__(self, *args, **kwargs):
        super(BaseStone, self).__init__(*args, **kwargs)
        self.velocity_x = MAX_BLUE_STONE_SPEED
        self.scale_x = 0.5
        self.scale_y = 0.5
        self.x = WINDOW_WIDTH

    def act(self, dt):
        self.x -= self.velocity_x * dt


class BlueStone(BaseStone):
    def __init__(self, y,  *args, **kwargs):
        super(BlueStone, self).__init__(img=pyglet.image.load("assets/blue_stone.png"), y=y, *args, **kwargs)


class GrayStone(BaseStone):
    def __init__(self, y,  *args, **kwargs):
        super(GrayStone, self).__init__(img=pyglet.image.load("assets/gray_stone.png"), y=y, *args, **kwargs)




